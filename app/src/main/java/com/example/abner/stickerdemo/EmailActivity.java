package com.example.abner.stickerdemo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class EmailActivity extends AppCompatActivity {

    EditText email;
    public String emailText = "";
    AlertDialog alert,alert2;
    TextView messageView;
    ProgressBar progressBar;

    //    String url = "https://api.mailgun.net/v3/tagbin.in/messages";
    View click;
    String mailurl = "http://tagplug.com/mailer/sendEmail.php";
   String url= "https://api:key-36284eefc213016fbf82f4478b69b797@api.mailgun.net/v3/tagbin.in/messages";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        email = (EditText) findViewById(R.id.editText);
        customDialog();
        click = findViewById(R.id.clickhere);
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                emailText = email.getText().toString();
//                SendSimpleMessage();

                if (emailText.equals("")){
                    email.setError("Enter Email");
                }else {
                    makeRequest();
                }

            }
        });
        click.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    click.setBackgroundColor(Color.parseColor("#FFC20914"));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    click.setBackgroundColor(Color.parseColor("#e41e2b"));
                }
                return false;
            }
        });

    }


//    public static ClientResponse SendSimpleMessage() {
//        Client client = Client.create();
//        client.addFilter(new HTTPBasicAuthFilter(
//                "api","key-36284eefc213016fbf82f4478b69b797"
//
//        ));
//        WebResource webResource = client.resource(
//                "https://api.mailgun.net/v3/tagbin.in/messages");
//        MultivaluedMapImpl formData = new MultivaluedMapImpl();
//        formData.add("from", "Coke Art < info@tagbin.in>");
//        formData.add("to", "ankit@tagbin.in");
//        formData.add("subject", "Coke Art");
//                formData.add("text", "Testing some Mailgun awesomeness!");
//                        formData.add("html", "Hello, <br>Here is you image from coke art.<br><br><img src='data:image/png;base64,[DATA]' />");
//        return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
//                post(ClientResponse.class, formData);
//    }

//
//    @Override
//    public Map<String, String> getHeaders() throws AuthFailureError {
//        HashMap<String, String> headers = new HashMap<String, String>();
//        headers.put("Content-Type", "application/json");
//        headers.put( "charset", "utf-8");
//        return headers;
//    }


    private void makeRequest() {

        showDialog();
        Log.v("Makerequest:%n %s", "worked");
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, mailurl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("Myresponse:%n %s", response.toString());
                        try {
                            String type= response.getString("type");
                            if (type.equals("success")){
                                dismissDialog();
                               responseDialog();
                            }else errorDialog("Check Email & Try Again!!");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("error:%n %s", error.toString());
                        errorDialog("Email Failed!! Try again!!");

                    }
                }) {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("from", "info@tagbin.in");
//                params.put("to", "ramandsb@gmail.com");
//                params.put("text", "sample text");
//                params.put("html", "Hello, <br>Here is you image from coke art.<br><br><img src='data:image/png;base64," + MainActivity.encodedImage + "' />");
                params.put("clientId", "22");
                params.put("email", emailText);
                params.put("id", "3");
                params.put("image", MainActivity.encodedImage);
                return params;
            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                headers.put("api_key", "key-36284eefc213016fbf82f4478b69b797");
//                headers.put("charset", "utf-8");
//                return headers;
//            }


        };
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsObjRequest);
//        requestQueue.cancelAll(tag);

    }
    public void customDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View customView = inflater.inflate(R.layout.dialog, null);
        builder.setView(customView);
        builder.setCancelable(true);
        messageView = (TextView) customView.findViewById(R.id.tvdialog);
        progressBar = (ProgressBar) customView.findViewById(R.id.progress);
        alert = builder.create();

    }
    public void responseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Email Sent");
        builder.setCancelable(true);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(EmailActivity.this,SplashScreen.class);
                startActivity(intent);
                finish();
            }
        });
        alert2 = builder.create();
        alert2.show();

    }


    public void showDialog() {
        progressBar.setVisibility(View.VISIBLE);
        alert.show();
        messageView.setText("Loading...");
    }

    public void dismissDialog() {
        alert.dismiss();
    }

    public void errorDialog(String error) {
        progressBar.setVisibility(View.GONE);
        messageView.setText("Error.. " + error);
    }

}
