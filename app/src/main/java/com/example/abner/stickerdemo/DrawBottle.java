package com.example.abner.stickerdemo;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class DrawBottle extends AppCompatActivity implements View.OnClickListener {
DrawingView drawView;
    private float extrasmallBrush,smallestbrush,smallBrush, mediumBrush, largeBrush;
    private RelativeLayout mContentRootView;
    private ImageButton currPaint, drawBtn, eraseBtn, newBtn, saveBtn, opacityBtn, redobut, undobut;
    long lastDown;
    long lastDuration;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_bottle);
        mContentRootView = (RelativeLayout) findViewById(R.id.rl_content_root);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        drawView = (DrawingView) findViewById(R.id.drawing);
        smallestbrush=getResources().getInteger(R.integer.smallest_brush);
        extrasmallBrush=getResources().getInteger(R.integer.extrasmall_size);
        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);
        drawView.setBrushSize(smallestbrush);
        drawView.setColor("#111111");
        drawBtn = (ImageButton) findViewById(R.id.draw_btn);
        drawBtn.setOnClickListener(this);
        eraseBtn = (ImageButton) findViewById(R.id.erase_btn);
        eraseBtn.setOnClickListener(this);
        drawBtn.setImageResource(R.drawable.pencil);
        eraseBtn.setImageResource(R.drawable.eraser);

        final View clickhere = findViewById(R.id.clickhere);
        clickhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(DrawBottle.this,MainActivity.class);
                startActivity(intent);
            }
        });


        // this goes wherever you setup your button listener:
        clickhere.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDown = System.currentTimeMillis();
                    clickhere.setBackgroundColor(Color.parseColor("#FFC20914"));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    clickhere.setBackgroundColor(Color.parseColor("#e41e2b"));
                }
                return false;
            }
        });


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.draw_btn) {

            drawView.setErase(false);
            drawView.setColor("#111111");
            drawBtn.setImageResource(R.drawable.pencilhighlighted);
            eraseBtn.setImageResource(R.drawable.eraser);
                    drawView.setBrushSize(smallestbrush);
                    drawView.setLastBrushSize(smallestbrush);
            //draw button clicked
//            final Dialog brushDialog = new Dialog(this);
//            brushDialog.setTitle("Brush size:");
//            brushDialog.setContentView(R.layout.brush_chooser);
//            //listen for clicks on size buttons
//            ImageButton smallest = (ImageButton) brushDialog.findViewById(R.id.smallest_brush);
//            smallest.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(false);
//                    drawView.setBrushSize(smallestbrush);
//                    drawView.setLastBrushSize(smallestbrush);
//                    brushDialog.dismiss();
//                }
//            });
//            ImageButton extrasmallBtn = (ImageButton) brushDialog.findViewById(R.id.extrasmall_brush);
//            extrasmallBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(false);
//                    drawView.setBrushSize(extrasmallBrush);
//                    drawView.setLastBrushSize(extrasmallBrush);
//                    brushDialog.dismiss();
//                }
//            });
//            ImageButton smallBtn = (ImageButton) brushDialog.findViewById(R.id.small_brush);
//            smallBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(false);
//                    drawView.setBrushSize(smallBrush);
//                    drawView.setLastBrushSize(smallBrush);
//                    brushDialog.dismiss();
//                }
//            });
//            ImageButton mediumBtn = (ImageButton) brushDialog.findViewById(R.id.medium_brush);
//            mediumBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(false);
//                    drawView.setBrushSize(mediumBrush);
//                    drawView.setLastBrushSize(mediumBrush);
//                    brushDialog.dismiss();
//                }
//            });
//            ImageButton largeBtn = (ImageButton) brushDialog.findViewById(R.id.large_brush);
//            largeBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(false);
//                    drawView.setBrushSize(largeBrush);
//                    drawView.setLastBrushSize(largeBrush);
//                    brushDialog.dismiss();
//                }
//            });
//            //show and wait for user interaction
//            brushDialog.show();
        } else if (view.getId() == R.id.erase_btn) {

            drawView.setErase(false);
            drawView.setColor("#ffffff");
            drawBtn.setImageResource(R.drawable.pencil);
            eraseBtn.setImageResource(R.drawable.eraserhighlighted);
            drawView.setBrushSize(mediumBrush);
            //switch to erase - choose size
//            final Dialog brushDialog = new Dialog(this);
//            brushDialog.setTitle("Eraser size:");
//            brushDialog.setContentView(R.layout.eraser_chooser);
//            //size buttons
//            ImageButton smallBtn = (ImageButton) brushDialog.findViewById(R.id.small_brush);
//            smallBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(true);
//                    drawView.setBrushSize(smallBrush);
//                    brushDialog.dismiss();
//                }
//            });
//            ImageButton mediumBtn = (ImageButton) brushDialog.findViewById(R.id.medium_brush);
//            mediumBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(true);
//                    drawView.setBrushSize(mediumBrush);
//                    brushDialog.dismiss();
//                }
//            });
//            ImageButton largeBtn = (ImageButton) brushDialog.findViewById(R.id.large_brush);
//            largeBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    drawView.setErase(true);
//                    drawView.setBrushSize(largeBrush);
//                    brushDialog.dismiss();
//                }
//            });
//            brushDialog.show();
        }
    }
}
