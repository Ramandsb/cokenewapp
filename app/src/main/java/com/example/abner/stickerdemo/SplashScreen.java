package com.example.abner.stickerdemo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class SplashScreen extends AppCompatActivity {
    private ProgressBar progressBar;
    private Handler mHandler = new Handler();
    View clickhere;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
         clickhere=  findViewById(R.id.clickhere);
        clickhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intenthome = new Intent(SplashScreen.this, DrawBottle.class);
            startActivity(intenthome);
            }
        });
        clickhere.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    clickhere.setBackgroundColor(Color.parseColor("#FFC20914"));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    clickhere.setBackgroundColor(Color.parseColor("#e41e2b"));
                }
                return false;
            }
        });
//        progressBar = (ProgressBar) findViewById(R.id.progress);
    }

    @Override
    public void onStart() {
        // When the aplication starts, show the progressbar for 2 seconds. After that, execute loadHomeActivity runnable.
//        long mStartTime = 0;
//        if (mStartTime == 0L) {
//            mStartTime = System.currentTimeMillis();
//            mHandler.removeCallbacks(loadHomeActivity);
//            progressBar.setVisibility(ProgressBar.VISIBLE);
//            progressBar.setProgress(0);
//            mHandler.postDelayed(loadHomeActivity, 3000);
//        }
        super.onStart();
    }
//
//    // A runnable executed when the progressbar finishes which starts the HomeActivity.
//    private Runnable loadHomeActivity = new Runnable() {
//        public void run() {
//            Intent intenthome = new Intent(SplashScreen.this, MainActivity.class);
//            startActivity(intenthome);
//        }
//
//    };


}
