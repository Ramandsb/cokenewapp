package com.example.abner.stickerdemo;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class DisplayActivity extends AppCompatActivity {

    private ImageView mDisplayImageView;
    View click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

         click= findViewById(R.id.clickhere);
     Button email= (Button) findViewById(R.id.emailbut);
        Button skip= (Button) findViewById(R.id.skip);
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DisplayActivity.this, EmailActivity.class);
                startActivity(intent);
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DisplayActivity.this,"Thank you",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DisplayActivity.this, SplashScreen.class);
                startActivity(intent);
                finish();
            }
        });
//        click.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    click.setBackgroundColor(Color.parseColor("#FFC20914"));
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    click.setBackgroundColor(Color.parseColor("#e41e2b"));
//                }
//                return false;
//            }
//        });

        mDisplayImageView = (ImageView) findViewById(R.id.iv_dis);
        String compoundPath = getIntent().getStringExtra("image");
        if (!TextUtils.isEmpty(compoundPath)) {
            mDisplayImageView.setImageURI(Uri.parse(compoundPath));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dislplay, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
